# mylinux
FROM fedora:27
LABEL author="edt 2022"
LABEL today="Monday"
RUN yum install -y procps nmap tree vim iputils iproute util-linux
WORKDIR /tmp
CMD /bin/bash
